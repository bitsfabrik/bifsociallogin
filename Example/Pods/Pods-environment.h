
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BIFSocialLogin
#define COCOAPODS_POD_AVAILABLE_BIFSocialLogin
#define COCOAPODS_VERSION_MAJOR_BIFSocialLogin 0
#define COCOAPODS_VERSION_MINOR_BIFSocialLogin 1
#define COCOAPODS_VERSION_PATCH_BIFSocialLogin 0

// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 0

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 14
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 1

// google-plus-ios-sdk
#define COCOAPODS_POD_AVAILABLE_google_plus_ios_sdk
#define COCOAPODS_VERSION_MAJOR_google_plus_ios_sdk 1
#define COCOAPODS_VERSION_MINOR_google_plus_ios_sdk 5
#define COCOAPODS_VERSION_PATCH_google_plus_ios_sdk 1

