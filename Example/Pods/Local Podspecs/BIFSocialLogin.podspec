Pod::Spec.new do |s|
  s.name             = "BIFSocialLogin"
  s.version          = "0.1.1"
  s.summary          = "A short description of BIFSocialLogin."
  s.description      = <<-DESC
                       An optional longer description of BIFSocialLogin

                       * Markdown format.
                       * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "http://EXAMPLE/NAME"
  s.license          = 'MIT'
  s.author           = { "Philip Messlehner" => "philip.messlehner@bitsfabrik.com" }
  s.source           = { :git => "http://EXAMPLE/NAME.git", :tag => s.version.to_s }

  s.platform     	 = :ios, '7.0'
  s.requires_arc 	 = true

  s.source_files 	 = 'Classes'

  s.dependency "Facebook-iOS-SDK", "~> 3.14", :inhibit_warnings => true
  s.dependency "google-plus-ios-sdk", "~> 1.5", :inhibit_warnings => true
end
