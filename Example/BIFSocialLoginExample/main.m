//
//  main.m
//  BIFSocialLoginExample
//
//  Created by Philip Messlehner on 27.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BIFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BIFAppDelegate class]));
    }
}
