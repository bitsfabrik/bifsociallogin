//
//  BIFAppDelegate.h
//  BIFSocialLoginExample
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
