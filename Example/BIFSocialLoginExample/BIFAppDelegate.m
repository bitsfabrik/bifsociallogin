//
//  BIFAppDelegate.m
//  BIFSocialLoginExample
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import "BIFAppDelegate.h"
#import <BIFSocialLogin.h>

@implementation BIFAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[BIFSocialLogin sharedInstance] setupForLoginProviders:BIFSocialLoginProviderFacebook|BIFSocialLoginProviderGooglePlus permissions:BIFSocialLoginPermissionUserProfile | BIFSocialLoginPermissionUserMail userInfo:@{BIFGooglePlusClientIDKey: @"279752206113-gdk2ssu25lvgl4nlmqouscmdecnu6m60.apps.googleusercontent.com"}];
    return YES;
}
							
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[BIFSocialLogin sharedInstance] handleOpenURL:url sourceApplicaiton:sourceApplication annotiation:annotation];
}

@end
