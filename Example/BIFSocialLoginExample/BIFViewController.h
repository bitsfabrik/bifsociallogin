//
//  BIFViewController.h
//  BIFSocialLoginExample
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BIFViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *googleButton;
@property (nonatomic, strong) IBOutlet UIButton *facebookButton;
@property (nonatomic, strong) IBOutlet UIButton *twitterButton;

- (IBAction)googleButtonPressed:(id)sender;
- (IBAction)facebookButtonPressed:(id)sender;
- (IBAction)twitterButtonPressed:(id)sender;

@end
