//
//  BIFViewController.m
//  BIFSocialLoginExample
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import "BIFViewController.h"
#import <BIFSocialLogin.h>
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface BIFViewController ()

@end

@implementation BIFViewController

- (void)viewDidLoad {
    [self.googleButton setTitle:@"Login with Google" forState:UIControlStateNormal];
    [self.googleButton setTitle:@"Logout from Google" forState:UIControlStateSelected];
    
    
    [self.facebookButton setTitle:@"Login with Facebook" forState:UIControlStateNormal];
    [self.facebookButton setTitle:@"Logout from Facebook" forState:UIControlStateSelected];
}

- (void)viewWillLayoutSubviews {
    self.googleButton.selected = [[BIFSocialLogin sharedInstance] authenticatedUserForProvider:BIFSocialLoginProviderGooglePlus] != nil;
    self.facebookButton.selected = [[BIFSocialLogin sharedInstance] authenticatedUserForProvider:BIFSocialLoginProviderFacebook] != nil;
}

- (IBAction)googleButtonPressed:(id)sender {
    if (((UIButton *)sender).selected) {
        [[BIFSocialLogin sharedInstance] disconnectFromProvider:BIFSocialLoginProviderGooglePlus];
    } else {
        [[BIFSocialLogin sharedInstance] authenticateForProvider:BIFSocialLoginProviderGooglePlus];
    }
}

- (IBAction)facebookButtonPressed:(id)sender {
    if (((UIButton *)sender).selected) {
        [[BIFSocialLogin sharedInstance] disconnectFromProvider:BIFSocialLoginProviderFacebook];
    } else {
        [[BIFSocialLogin sharedInstance] authenticateForProvider:BIFSocialLoginProviderFacebook];
    }
}

- (IBAction)twitterButtonPressed:(id)sender {
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
        ACAccount *twitterAccount = [[accountStore accountsWithAccountType:accountType] firstObject];
        if (granted && twitterAccount != nil) {
            NSLog(@"Succeeded with twitterAccount %@", twitterAccount);
        } else {
            NSLog(@"Failed with twitterAccount %@", twitterAccount);
        }
    }];
}

@end
