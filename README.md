# BIFSocialLogin

[![Version](https://img.shields.io/cocoapods/v/BIFSocialLogin.svg?style=flat)](http://cocoadocs.org/docsets/BIFSocialLogin)
[![License](https://img.shields.io/cocoapods/l/BIFSocialLogin.svg?style=flat)](http://cocoadocs.org/docsets/BIFSocialLogin)
[![Platform](https://img.shields.io/cocoapods/p/BIFSocialLogin.svg?style=flat)](http://cocoadocs.org/docsets/BIFSocialLogin)

## Usage

To run the example project; clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

BIFSocialLogin is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "BIFSocialLogin"

## Author

Philip Messlehner, mess.philip@gmail.com

## License

BIFSocialLogin is available under the MIT license. See the LICENSE file for more info.

