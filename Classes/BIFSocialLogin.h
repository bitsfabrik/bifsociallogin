//
//  BIFSocialLogin.h
//  BIFSocialLogin
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const BIFGooglePlusClientIDKey;

extern NSString * const BIFSocialLoginFinishedNotification;
extern NSString * const BIFSocialLoginDisconnectedNotification;
extern NSString * const BIFSocialLoginNotificationProviderKey;
extern NSString * const BIFSocialLoginNotificationUserKey;
extern NSString * const BIFSocialLoginNotificationErrorKey;

typedef NS_OPTIONS (NSUInteger, BIFSocialLoginPermissions) {
    BIFSocialLoginPermissionUserProfile,
    BIFSocialLoginPermissionUserMail,
    BIFSocialLoginPermissionFriends
};

typedef NS_OPTIONS (NSUInteger, BIFSocialLoginProviders) {
    BIFSocialLoginProviderNone = 0,
    BIFSocialLoginProviderFacebook = 1 << 0,
    BIFSocialLoginProviderGooglePlus = 1 << 1
};

@interface BIFSocialLoginUser : NSObject

@property (nonatomic, strong, readonly) id userID;
@property (nonatomic, copy, readonly) NSString *firstName;
@property (nonatomic, copy, readonly) NSString *lastName;
@property (nonatomic, copy, readonly) NSString *username;
@property (nonatomic, copy, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSURL *imageURL;
@property (nonatomic, strong, readonly) id authenticationData;
@property (nonatomic, strong, readonly) NSString *activeSessionToken;

@end

@interface BIFSocialLogin : NSObject

@property (nonatomic, assign, readonly) BIFSocialLoginProviders loginProviders;
@property (nonatomic, assign, readonly) BIFSocialLoginPermissions permissions;

+ (instancetype)sharedInstance;

- (void)setupForLoginProviders:(BIFSocialLoginProviders)providers permissions:(BIFSocialLoginPermissions)permissions userInfo:(NSDictionary *)userInfo;
- (void)setupForLoginProviders:(BIFSocialLoginProviders)providers permissions:(BIFSocialLoginPermissions)permissions;
- (BOOL)handleOpenURL:(NSURL *)url sourceApplicaiton:(NSString *)sourceApplication annotiation:(id)annotation;
- (BIFSocialLoginUser *)authenticatedUserForProvider:(BIFSocialLoginProviders)provider;
- (void)authenticateForProvider:(BIFSocialLoginProviders)provider;
- (void)disconnectFromProvider:(BIFSocialLoginProviders)provider;

@end
