//
//  BIFSocialLogin.m
//  BIFSocialLogin
//
//  Created by Philip Messlehner on 26.05.14.
//  Copyright (c) 2014 bitsfabrik GmbH. All rights reserved.
//

#import "BIFSocialLogin.h"
#import "Facebook.h"
#import "GooglePlus.h"
#import "GTLPlusPerson.h"

NSString * const BIFGooglePlusClientIDKey = @"BIFGooglePlusClientIDKey";
NSString * const BIFSocialLoginFinishedNotification = @"BIFSocialLoginFinishedNotification";
NSString * const BIFSocialLoginDisconnectedNotification = @"BIFSocialLoginDisconnectedNotification";
NSString * const BIFSocialLoginNotificationProviderKey = @"BIFSocialLoginNotificationProviderKey";
NSString * const BIFSocialLoginNotificationUserKey = @"BIFSocialLoginNotificationUserKey";
NSString * const BIFSocialLoginNotificationErrorKey = @"BIFSocialLoginNotificationErrorKey";

@interface BIFSocialLoginUser ()

@property (nonatomic, strong, readwrite) id userID;
@property (nonatomic, copy, readwrite) NSString *firstName;
@property (nonatomic, copy, readwrite) NSString *lastName;
@property (nonatomic, copy, readwrite) NSString *username;
@property (nonatomic, copy, readwrite) NSString *email;
@property (nonatomic, strong, readwrite) NSURL *imageURL;
@property (nonatomic, strong, readwrite) id authenticationData;
@property (nonatomic, strong, readwrite) NSString *activeSessionToken;

@end

@implementation BIFSocialLoginUser

@end

@interface BIFSocialLogin () <GPPSignInDelegate>

/** Expose readonly Properties */
@property (nonatomic, assign, readwrite) BIFSocialLoginProviders loginProviders;
@property (nonatomic, assign, readwrite) BIFSocialLoginPermissions permissions;

/** Authentication Data */
@property (nonatomic, strong) BIFSocialLoginUser *googleUser;
@property (nonatomic, strong) BIFSocialLoginUser *facebookUser;

/** Helper **/
@property (nonatomic, strong, readonly) NSArray *facebookPermissions;

@end

@implementation BIFSocialLogin

////////////////////////////////////////////////////////////
#pragma mark - Life Cycle
////////////////////////////////////////////////////////////

+ (instancetype)sharedInstance {
    static dispatch_once_t onceToken;
    static BIFSocialLogin *sharedInstance;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self != nil) {
        [GPPSignIn sharedInstance].delegate = self;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
    }
    return self;
}

////////////////////////////////////////////////////////////
#pragma mark - BIFSocialLogin (public)
////////////////////////////////////////////////////////////

- (void)setupForLoginProviders:(BIFSocialLoginProviders)loginProviders permissions:(BIFSocialLoginPermissions)permissions userInfo:(NSDictionary *)userInfo {
    self.permissions = permissions;
    self.loginProviders = loginProviders;
    if (self.facebookLoginEnabled) {
        if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
            [FBSession openActiveSessionWithReadPermissions:self.facebookPermissions
                                               allowLoginUI:NO
                                          completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
                                              [self facebookSessionStateChanged:session state:state error:error];
                                          }];
        }
    }
    
    if (self.googlePlusLoginEnabled) {
        NSString *clientID = userInfo[BIFGooglePlusClientIDKey];
        if (clientID == nil) {
            return;
        }
        
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        NSMutableArray *scopes = [NSMutableArray array];
        [scopes addObject:@"profile"];
        
        if ((permissions & BIFSocialLoginPermissionUserMail) != 0) {
            signIn.shouldFetchGoogleUserEmail = YES;
            [scopes addObject:@"email"];
        }
        
        if ((permissions & BIFSocialLoginPermissionFriends) != 0) {
            signIn.shouldFetchGooglePlusUser = YES;
            [scopes addObject:@"https://www.googleapis.com/auth/plus.login"];
        }
        
        // You previously set kClientId in the "Initialize the Google+ client" step
        signIn.clientID = clientID;
        
        // Uncomment one of these two statements for the scope you chose in the previous step
        signIn.scopes = scopes;
        
        [signIn trySilentAuthentication];
    }
}

- (void)setupForLoginProviders:(BIFSocialLoginProviders)loginProviders permissions:(BIFSocialLoginPermissions)permissions {
    [self setupForLoginProviders:loginProviders permissions:permissions userInfo:nil];
}

- (BIFSocialLoginUser *)authenticatedUserForProvider:(BIFSocialLoginProviders)provider {
    if ((provider & BIFSocialLoginProviderFacebook) != 0 && (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)) {
        return self.facebookUser;
    } else if ((provider & BIFSocialLoginProviderGooglePlus) != 0 && [[GPPSignIn sharedInstance] authentication] != nil) {
        return self.googleUser;
    }
    return nil;
}

- (void)authenticateForProvider:(BIFSocialLoginProviders)provider {
    if ((provider & BIFSocialLoginProviderFacebook) != 0) {
        [FBSession openActiveSessionWithReadPermissions:self.facebookPermissions
                                           allowLoginUI:YES
                                      completionHandler:
         ^(FBSession *session, FBSessionState state, NSError *error) {
             [self facebookSessionStateChanged:session state:state error:error];
         }];
    } else if ((provider & BIFSocialLoginProviderGooglePlus) != 0) {
        [[GPPSignIn sharedInstance] authenticate];
    }
}

- (void)disconnectFromProvider:(BIFSocialLoginProviders)provider {
    if ((provider & BIFSocialLoginProviderFacebook) != 0) {
        [FBSession.activeSession closeAndClearTokenInformation];
        self.facebookUser = nil;
    } else {
        [[GPPSignIn sharedInstance] disconnect];
        self.googleUser = nil;
    }
}

- (BOOL)handleOpenURL:(NSURL *)url sourceApplicaiton:(NSString *)sourceApplication annotiation:(id)annotation {
    [FBSession.activeSession setStateChangeHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         [self facebookSessionStateChanged:session state:state error:error];
     }];
    
    BOOL success = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    if (!success) {
        success = [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    }
    return success;
}

////////////////////////////////////////////////////////////
#pragma mark - BIFSocialLogin (private Helper)
////////////////////////////////////////////////////////////

- (BOOL)facebookLoginEnabled {
    return ((self.loginProviders & BIFSocialLoginProviderFacebook) != 0);
}

- (BOOL)googlePlusLoginEnabled {
    return ((self.loginProviders & BIFSocialLoginProviderGooglePlus) != 0);
}

////////////////////////////////////////////////////////////
#pragma mark - BIFSocialLogin (Facebook Helper)
////////////////////////////////////////////////////////////

- (NSArray *)facebookPermissions {
    NSMutableArray *facebookPermissions = [NSMutableArray arrayWithObject:@"public_profile"];
    if ((self.permissions & BIFSocialLoginPermissionUserMail) != 0) {
        [facebookPermissions addObject:@"email"];
    }
    if ((self.permissions & BIFSocialLoginPermissionFriends) != 0) {
        [facebookPermissions addObject:@"user_friends"];
    }
    return facebookPermissions;
}

- (void)applicationDidBecomeActiveNotification:(NSNotification *)notification {
    [FBAppCall handleDidBecomeActive];
}

- (void)facebookSessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error {
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (error == nil) {
                BIFSocialLoginUser *facebookUser = [BIFSocialLoginUser new];
                facebookUser.userID = result[@"id"];
                facebookUser.username = result[@"name"];
                facebookUser.firstName = result[@"first_name"];
                facebookUser.lastName = result[@"last_name"];
                facebookUser.email = result[@"email"];
                facebookUser.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://graph.facebook.com/%@/picture", facebookUser.userID]];
                facebookUser.authenticationData = result;
                facebookUser.activeSessionToken = session.accessTokenData.accessToken;
                self.facebookUser = facebookUser;
                [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginFinishedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderFacebook), BIFSocialLoginNotificationUserKey: self.facebookUser}];
            } else {
                NSLog(@"Error : %@", error);
                self.facebookUser = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginDisconnectedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderFacebook), BIFSocialLoginNotificationErrorKey: error}];
            }
        }];
        return;
    }
    
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        NSLog(@"Session closed");
    }
    
    if (error != nil){
        NSLog(@"Error");
        NSString *alertText = nil;
        NSString *alertTitle = nil;
        
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            // If the error requires people using an app to make an action outside of the app in order to recover
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
        } else {
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
            } else {
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
            }
        }
        NSLog(@"Error while logging into Facebook: %@ - %@", alertTitle, alertText);
        
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginDisconnectedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderFacebook), BIFSocialLoginNotificationErrorKey: error}];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginDisconnectedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderFacebook)}];
    }
    self.facebookUser = nil;
}

////////////////////////////////////////////////////////////
#pragma mark - GPPSignInDelegate
////////////////////////////////////////////////////////////

- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error {
    NSLog(@"Finished %@ Error: %@", auth, error);
    if (auth != nil && error != nil) {
        BIFSocialLoginUser *googleUser = [BIFSocialLoginUser new];
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        googleUser.userID = signIn.userID;
        googleUser.email = signIn.userEmail;
        googleUser.username = signIn.googlePlusUser.nickname;
        googleUser.firstName = signIn.googlePlusUser.name.givenName;
        googleUser.lastName = signIn.googlePlusUser.name.familyName;
        googleUser.imageURL = [NSURL URLWithString:signIn.googlePlusUser.image.url];
        googleUser.authenticationData = auth;
        self.googleUser = googleUser;
        [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginFinishedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderFacebook), BIFSocialLoginNotificationUserKey: self.googleUser}];
    } else {
        self.googleUser = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginDisconnectedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderGooglePlus), BIFSocialLoginNotificationErrorKey: error}];
    }
}

- (void)didDisconnectWithError:(NSError *)error {
    self.googleUser = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:BIFSocialLoginDisconnectedNotification object:self userInfo:@{BIFSocialLoginNotificationProviderKey: @(BIFSocialLoginProviderGooglePlus), BIFSocialLoginNotificationErrorKey: error}];
}

@end
