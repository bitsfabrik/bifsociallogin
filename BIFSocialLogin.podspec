Pod::Spec.new do |s|
  s.name             = "BIFSocialLogin"
  s.version          = "0.1.3"
  s.summary          = "Login Handler for Facebook and Google Plus."
  s.description      = <<-DESC
                       Wrapper for Facebook and Google Plus Login to
					   easily integrate them into your Application
                       DESC
  s.homepage         = "https://bitbucket.org/bitsfabrik/bifsociallogin"
  s.license          = 'MIT'
  s.author           = { "Philip Messlehner" => "philip.messlehner@bitsfabrik.com" }
  s.source           = { :git => "https://bitbucket.org/bitsfabrik/bifsociallogin", :tag => s.version.to_s }

  s.platform     	 = :ios, '7.0'
  s.requires_arc 	 = true

  s.source_files 	 = 'Classes'

  s.dependency "Facebook-iOS-SDK", "~> 3.14"
  s.dependency "google-plus-ios-sdk", "~> 1.5"
end
